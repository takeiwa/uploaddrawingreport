﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Reflection;
using NLog;
using System;

namespace UploadDrawingReport
{
    class Program
    {
        static HttpClient client = new HttpClient();

        private static Logger _logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            Console.WriteLine("Start Upload");

            //Get the current folder path
            var appPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            //folder for delta(delta_beam/delta_report)
            var reportPath = $"{appPath}\\Report";
            var sendPath = $"{appPath}\\Send";
            if(!Directory.Exists(reportPath))
            {
                Directory.CreateDirectory(reportPath);
                Console.WriteLine("Make directory of input.");
                _logger.Info($"No data file.");
                return;
            }
            if (!Directory.Exists(sendPath))
            {
                Directory.CreateDirectory(sendPath);
                Directory.CreateDirectory($"{sendPath}\\backup");
                Console.WriteLine("Make directory of output.");
            }

            //Repeat for the number of files
            string[] files = Directory.GetFiles(reportPath);

            for (int i = 0; i < files.Length; i++)
            {
                //Branch by file name
                string fileName = Path.GetFileName(files[i]);
                Console.WriteLine(fileName + " Uploading...");

                if (fileName.IndexOf("BeamRecode") > -1)
                {
                    //drawing api delta_beam
                    UploadDeltaBeamFileAsync(files[i], sendPath).GetAwaiter().GetResult();
                }
                else
                {
                    //drawing api delta_report
                    UploadDeltaReportFileAsync(files[i], sendPath).GetAwaiter().GetResult();
                }
            }
            Console.WriteLine("Finished.");
            //Console.Read();
        }   

        static async Task UploadDeltaBeamFileAsync(string filePath, string movePath)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            var dic = new Dictionary<string, string>();
            dic["type"] = "delta_beam";
            //dic["test"] = "1";
            //dic["dump"] = "0";

            var fileName = Path.GetFileName(filePath);  //ファイルパス
            var fileContent = new StreamContent(File.OpenRead(filePath));
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                Name = "file",
                FileName = fileName
            };

            var content = new MultipartFormDataContent();
            foreach (var param in dic)
            {
                content.Add(new StringContent(param.Value), param.Key);
            }
            content.Add(fileContent);

            var url = "http://52.197.251.166/machines/reports/report";
            var req = await client.PostAsync(url, content);

            var html = await req.Content.ReadAsStringAsync();

            if (req.IsSuccessStatusCode)
            {
                Console.WriteLine("Success!");
                _logger.Info($"[delta_beam] " + fileName + ": " + "Success.");
                //_logger.Debug($"Details: " + html);
                //move file
                if (!File.Exists(movePath + "\\" + fileName))
                {
                    File.Move(filePath, movePath + "\\" + fileName);
                }
                else
                {
                    File.Move(filePath, movePath + "\\backup\\" + fileName);
                }
            }
            else
            {
                Console.WriteLine("Failure...");
                _logger.Error($"[delta_beam] " + fileName + ": " + html);
            }
        }

        static async Task UploadDeltaReportFileAsync(string filePath, string movePath)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            var dic = new Dictionary<string, string>();
            dic["type"] = "delta_report";
            dic["exclusion_time"] = "0";
            //dic["test"] = "1";
            //dic["dump"] = "0";

            var fileName = Path.GetFileName(filePath);  //ファイルパス
            var fileContent = new StreamContent(File.OpenRead(filePath));
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                Name = "file",
                FileName = fileName
            };

            var content = new MultipartFormDataContent();
            foreach (var param in dic)
            {
                content.Add(new StringContent(param.Value), param.Key);
            }
            content.Add(fileContent);

            var url = "http://52.197.251.166/machines/reports/report";
            var req = await client.PostAsync(url, content);

            var html = await req.Content.ReadAsStringAsync();

            if (req.IsSuccessStatusCode)
            {
                Console.WriteLine("Success!");
                _logger.Info($"[delta_report] " + fileName + ": " + "Success.");
                //_logger.Debug($"Details: " + html);
                //move file
                if (!File.Exists(movePath + "\\" + fileName))
                {
                    File.Move(filePath, movePath + "\\" + fileName);
                }
                else
                {
                    File.Move(filePath, movePath + "\\backup\\" + fileName);
                }
            }
            else
            {
                Console.WriteLine("Failure...");
                _logger.Error($"[delta_report] " + fileName + ": " + html);
            }
        }
    }
}
